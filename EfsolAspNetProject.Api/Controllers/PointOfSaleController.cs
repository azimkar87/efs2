﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfsolAspNetProject.Core;
using EfsolAspNetProject.DAL;
using EfsolAspNetProject.Domain.Dtos;
using EfsolAspNetProject.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace EfsolAspNetProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PointOfSaleController : ControllerBase
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFoodService _foodService;

        public PointOfSaleController(IUnitOfWorkFactory unitOfWorkFactory, IFoodService foodService)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _foodService = foodService;
        }

        // GET api/pointOfSale
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var food = unitOfWork.Food.GetAll();

                return Ok(food);
            }
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] FoodCreateDto foodDto)
        {
            var result = await _foodService.CreatePointOfSaleAsync(foodDto);

            if (result.IsSuccess)
            {
                return Ok(result.Entity);
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
