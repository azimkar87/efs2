﻿using EfsolAspNetProject.Core.Models;

namespace EfsolAspNetProject.Core.Repositories
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {

    }
}