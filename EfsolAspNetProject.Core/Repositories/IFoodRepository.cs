﻿using EfsolAspNetProject.Core.Models;

namespace EfsolAspNetProject.Core.Repositories
{
    public interface IFoodRepository : IRepository<Food>
    {
        Food GetByName(string foodName);
    }
}
