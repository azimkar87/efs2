﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EfsolAspNetProject.Core.Models;

namespace EfsolAspNetProject.Core.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        Task<T> AddAsync(T entity);
        IEnumerable<T> GetAll();
        Task<IEnumerable<T>> GetAllAsync();
        T GetById(int id);
        void Update(T entity);
        void Delete(T entity);
    }
}
