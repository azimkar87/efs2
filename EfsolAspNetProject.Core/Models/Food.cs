﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EfsolAspNetProject.Core.Models
{
    public class Food : Entity
    {
        [Display(Name = "Наименование")]
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int RestaurantId { get; set; }
        [Display(Name = "Ресторан")]
        [JsonIgnore]
        public Restaurant Restaurant { get; set; }
    }
}
