﻿using System.Collections.Generic;


namespace EfsolAspNetProject.Core.Models
{
    public class Restaurant : Entity
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }

        public ICollection<Food> Food { get; set; }

        public Restaurant()
        {
            Food = new List<Food>();
        }
    }
}
