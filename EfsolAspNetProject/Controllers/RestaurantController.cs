﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfsolAspNetProject.DAL;
using Microsoft.AspNetCore.Mvc;

namespace EfsolAspNetProject.Controllers
{
    public class RestaurantController : Controller
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RestaurantController(IUnitOfWorkFactory unitOfWorkFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public IActionResult Index()
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var restaurants = unitOfWork.Restaurants.GetAll();
                return View(restaurants);
            }
        }

        // GET: foods/Details
        public async Task<IActionResult> Details(int? id)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                if (!id.HasValue)
                {
                    return NotFound();
                }

                var food = unitOfWork.Restaurants.GetById(id.Value);
                if (food == null)
                {
                    return NotFound();
                }

                return View(food);
            }
        }

    }
}