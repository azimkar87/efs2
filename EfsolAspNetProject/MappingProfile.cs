﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EfsolAspNetProject.Core.Models;
using EfsolAspNetProject.Domain.Dtos;
using EfsolAspNetProject.ViewModels;

namespace EfsolAspNetProject
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            FoodMapping();
        }

        private void FoodMapping()
        {
            CreateMap<FoodCreateModel, Food>();
            CreateMap<Food, FoodEditModel>();
            CreateMap<FoodEditModel, Food>();
            CreateMap<FoodCreateModel, FoodCreateDto>();
            CreateMap<FoodCreateDto, Food>();
        }
    }
}
