﻿using System.ComponentModel.DataAnnotations;

namespace EfsolAspNetProject.Domain.Dtos
{
    public class FoodCreateDto
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int RestaurantId { get; set; }
    }
}
