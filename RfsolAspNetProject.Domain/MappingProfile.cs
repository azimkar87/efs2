﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using EfsolAspNetProject.Core.Models;
using EfsolAspNetProject.Domain.Dtos;

namespace EfsolAspNetProject.Domain
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            FoodMapping();
        }

        private void FoodMapping()
        {
            CreateMap<FoodCreateDto, Food>();
        }
    }
}
