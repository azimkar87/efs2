﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using EfsolAspNetProject.Core.Models;
using EfsolAspNetProject.DAL;
using FoodCreateDto = EfsolAspNetProject.Domain.Dtos.FoodCreateDto;

namespace EfsolAspNetProject.Domain.Services
{
    public interface IFoodService
    {
        Task<EntityOperationResult<Food>> CreatePointOfSaleAsync(FoodCreateDto createDto);
    }

    public class FoodService : IFoodService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public FoodService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if(unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public async Task<EntityOperationResult<Food>> CreatePointOfSaleAsync(FoodCreateDto createDto)
        {
            using (var unitOfWork = _unitOfWorkFactory.MakeUnitOfWork())
            {
                var pos = unitOfWork.Food.GetByName(createDto.Name);

                if (pos != null)
                    return EntityOperationResult<Food>
                        .Failure()
                        .AddError($"<Блюдо с именем {createDto.Name} уже существует");

                try
                {
                    var food = Mapper.Map<Food>(createDto);

                    var entity = await unitOfWork.Food.AddAsync(food);
                    await unitOfWork.CompleteAsync();

                    return EntityOperationResult<Food>.Success(entity);
                }
                catch (Exception ex)
                {
                    return EntityOperationResult<Food>.Failure().AddError(ex.Message);
                }
            }
        }
    }
}
