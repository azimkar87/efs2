﻿using System;
using System.Threading.Tasks;
using EfsolAspNetProject.Core;
using EfsolAspNetProject.Core.Models;
using EfsolAspNetProject.Core.Repositories;

namespace EfsolAspNetProject.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IRestaurantRepository Restaurants { get; }
        IFoodRepository Food { get; }

        Task<int> CompleteAsync();
        void BeginTransaction();
        //void BeginTransaction(IsolationLevel level);
        void RollbackTransaction();
        void CommitTransaction();

        IRepository<TEntity> GetRepository<TEntity>() where TEntity : Entity;
    }
}
