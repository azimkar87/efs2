﻿namespace EfsolAspNetProject.DAL
{
    public interface IUnitOfWorkFactory
    {
        IUnitOfWork MakeUnitOfWork();
    }
}
