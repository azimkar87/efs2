﻿using EfsolAspNetProject.Core.Models;
using Microsoft.EntityFrameworkCore;

namespace EfsolAspNetProject.DAL.DbContext
{
    public class ApplicationDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<Food> Food { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
