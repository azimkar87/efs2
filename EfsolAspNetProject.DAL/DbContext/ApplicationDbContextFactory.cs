﻿using EfsolAspNetProject.DAL.DbContext.Contracts;
using Microsoft.EntityFrameworkCore;

namespace EfsolAspNetProject.DAL.DbContext
{
    public class ApplicationDbContextFactory : IApplicationDbContextFactory
    {
        private readonly DbContextOptions<ApplicationDbContext> _options;

        public ApplicationDbContextFactory(
            DbContextOptions<ApplicationDbContext> options)
        {
            _options = options;
        }

        public ApplicationDbContext Create()
        {
            return new ApplicationDbContext(_options);
        }
    }
}