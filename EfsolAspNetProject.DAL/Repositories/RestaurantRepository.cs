﻿using EfsolAspNetProject.Core.Models;
using EfsolAspNetProject.Core.Repositories;
using EfsolAspNetProject.DAL.DbContext;

namespace EfsolAspNetProject.DAL.Repositories
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(ApplicationDbContext context) : base(context)
        {
            DbSet = context.Restaurants;
        }
    }
}
